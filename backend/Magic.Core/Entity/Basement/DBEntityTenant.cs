﻿
using SqlSugar;

namespace Magic.Core.Entity;

/// <summary>
/// 自定义租户基类实体
/// </summary>
public abstract class DBEntityTenant : DEntityBase, ITenantId
{
    [SugarColumn(ColumnDescription = "租户id", IsNullable = true)]
    public long? TenantId { get; set; }
}
