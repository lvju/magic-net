﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core.Entity;

/// <summary>
/// 租户id全局过滤器
/// </summary>
public interface ITenantId
{
    
    public long? TenantId { get; set; }
}
