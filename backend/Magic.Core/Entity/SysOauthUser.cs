﻿using SqlSugar;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Magic.Core.Entity;

/// <summary>
/// Oauth登录用户表
/// </summary>
[SugarTable("sys_oauth_user")]
[Description("Oauth登录用户表")]
public class SysOauthUser : DEntityBase
{
    /// <summary>
    /// 第三方平台的用户唯一Id
    /// </summary>
    [MaxLength(64)]
    [SugarColumn(ColumnDescription = "第三方平台的用户唯一Id", IsNullable = true, Length = 64)]
    public string Uuid { get; set; }

    /// <summary>
    /// 用户授权的token
    /// </summary>
    [SugarColumn(ColumnDescription = "用户授权的token", IsNullable = true)]
    public string AccessToken { get; set; }

    /// <summary>
    /// 昵称
    /// </summary>
    [MaxLength(32)]
    [SugarColumn(ColumnDescription = "昵称", IsNullable = true, Length = 32)]
    public string NickName { get; set; }

    /// <summary>
    /// 头像
    /// </summary>
    [SugarColumn(ColumnDescription = "头像", IsNullable = true,ColumnDataType =StaticConfig.CodeFirst_BigString)]
    public string Avatar { get; set; }

    /// <summary>
    /// 性别
    /// </summary>
    [MaxLength(16)]
    [SugarColumn(ColumnDescription = "性别", IsNullable = true, Length = 16)]
    public string Gender { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    [MaxLength(32)]
    [SugarColumn(ColumnDescription = "电话", IsNullable = true, Length = 32)]
    public string Phone { get; set; }

    /// <summary>
    /// 邮箱
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "邮箱", IsNullable = true, Length = 255)]
    public string Email { get; set; }

    /// <summary>
    /// 位置
    /// </summary>
    [MaxLength(64)]
    [SugarColumn(ColumnDescription = "位置", IsNullable = true, Length = 64)]
    public string Location { get; set; }

    /// <summary>
    /// 用户网址
    /// </summary>
    [SugarColumn(ColumnDescription = "用户网址", IsNullable = true, Length = 255)]
    public string Blog { get; set; }

    /// <summary>
    /// 所在公司
    /// </summary>
    [MaxLength(64)]
    [SugarColumn(ColumnDescription = "所在公司", IsNullable = true, Length = 64)]
    public string Company { get; set; }

    /// <summary>
    /// 用户来源
    /// </summary>
    [MaxLength(32)]
    [SugarColumn(ColumnDescription = "用户来源", IsNullable = true, Length = 32)]
    public string Source { get; set; }

    /// <summary>
    /// 用户备注（各平台中的用户个人介绍）
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "用户备注（各平台中的用户个人介绍）", IsNullable = true, Length = 255)]
    public string Remark { get; set; }
}
