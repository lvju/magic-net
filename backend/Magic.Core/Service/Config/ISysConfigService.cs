﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysConfigService: ITransient
{
    Task Add(AddSysConfigInput input);
    Task Delete(Core.PrimaryKeyParam input);
    Task<SysConfig> Get(Core.PrimaryKeyParam input);
    Task<List<SysConfig>> List(QuerySysConfigPageInput input);
    Task<PageList<SysConfig>> PageList(QuerySysConfigPageInput input);
    Task Update(EditSysConfigInput input);
    Task<bool> GetDemoEnvFlag();
    Task<bool> GetCaptchaOpenFlag();

    Task<string> GetDefaultPassword();
    Task UpdateConfigCache(string code, object value);
}
