﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Magic.Core.Service;

/// <summary>
/// 登录输入参数
/// </summary>
[SuppressSniffer]
public class LoginInput
{
    /// <summary>
    /// 租户id
    /// </summary>
    /// <example>superAdmin</example>
    public long? TenantId { get; set; }
    /// <summary>
    /// 用户名
    /// </summary>
    /// <example>superAdmin</example>
    [Required(ErrorMessage = "用户名不能为空"), MinLength(3, ErrorMessage = "用户名不能少于3位字符")]
    public string Account { get; set; }

    /// <summary>
    /// 密码
    /// </summary>
    /// <example>123456</example>
    [Required(ErrorMessage = "密码不能为空"), MinLength(5, ErrorMessage = "密码不能少于5位字符")]
    public string Password { get; set; }
}


/// <summary>
/// 用户登录输出参数
/// </summary>
[SuppressSniffer]
public class LoginOutput
{
    /// <summary>
    /// 主键
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 账号
    /// </summary>
    public string Account { get; set; }

    /// <summary>
    /// 昵称
    /// </summary>
    public string NickName { get; set; }

    /// <summary>
    /// 姓名
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 头像
    /// </summary>
    public string Avatar { get; set; }

    /// <summary>
    /// 生日
    /// </summary>
    public DateTime Birthday { get; set; }

    /// <summary>
    /// 性别(字典 1男 2女)
    /// </summary>
    public int Sex { get; set; }

    /// <summary>
    /// 邮箱
    /// </summary>
    public String Email { get; set; }

    /// <summary>
    /// 手机
    /// </summary>
    public String Phone { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    public String Tel { get; set; }

    /// <summary>
    /// 管理员类型（0超级管理员 1非管理员）
    /// </summary>
    public int AdminType { get; set; }

    /// <summary>
    /// 最后登陆IP
    /// </summary>
    public string LastLoginIp { get; set; }

    /// <summary>
    /// 最后登陆时间
    /// </summary>
    public DateTime LastLoginTime { get; set; }

    /// <summary>
    /// 最后登陆地址
    /// </summary>
    public string LastLoginAddress { get; set; }

    /// <summary>
    /// 最后登陆所用浏览器
    /// </summary>
    public string LastLoginBrowser { get; set; }

    /// <summary>
    /// 最后登陆所用系统
    /// </summary>
    public string LastLoginOs { get; set; }

    /// <summary>
    /// 员工信息
    /// </summary>
    public EmpOutput LoginEmpInfo { get; set; } = new EmpOutput();

    /// <summary>
    /// 具备应用信息
    /// </summary>
    public List<SysApp> Apps { get; set; } = new List<SysApp>();

    /// <summary>
    /// 角色信息
    /// </summary>
    public List<RoleOutput> Roles { get; set; } = new List<RoleOutput>();

    /// <summary>
    /// 权限信息
    /// </summary>
    public List<string> Permissions { get; set; } = new List<string>();

    /// <summary>
    /// 登录菜单信息---AntDesign版本菜单
    /// </summary>
    public List<AntDesignTreeNode> Menus { get; set; } = new List<AntDesignTreeNode>();

    /// <summary>
    /// 数据范围（机构）信息
    /// </summary>
    public List<long> DataScopes { get; set; } = new List<long>();

}


