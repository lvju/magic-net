﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public class QueryDictPageInput:PageParamBase
{
    /// <summary>
    /// 名称
    /// </summary>
    public virtual string Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public virtual string Code { get; set; }
}

public class AddDictTypeInput
{
    /// <summary>
    /// 名称
    /// </summary>
    [Required(ErrorMessage = "字典类型名称不能为空")]
    public  string Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [Required(ErrorMessage = "字典类型编码不能为空")]
    public  string Code { get; set; }

    public List<AddDictDataInput> DictDataList { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public virtual int Sort { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public virtual string Remark { get; set; }
}

public class EditDictTypeInput : AddDictTypeInput
{ 
    public long Id { get; set; }
}

public class UpdateDictTypeStatusInput {
    public long Id { get; set; }


    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    public CommonStatus Status { get; set; }
}

public class QueryDropDownDictTypeInput {
    /// <summary>
    /// 编码
    /// </summary>
    [Required(ErrorMessage = "字典类型编码不能为空")]
    public string Code { get; set; }
}

