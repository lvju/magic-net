﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core.Service;

#region 输入参数

/// <summary>
/// 枚举输入参数
/// </summary>
public class EnumDataInput
{
    /// <summary>
    /// 枚举类型名称
    /// </summary>
    /// <example>Gender</example>
    [Required(ErrorMessage = "枚举类型不能为空")]
    public string EnumName { get; set; }
}

public class QueryEnumDataInput
{
    /// <summary>
    /// 实体名称
    /// </summary>
    [Required(ErrorMessage = "实体名称不能为空")]
    public string EntityName { get; set; }

    /// <summary>
    /// 字段名称
    /// </summary>
    [Required(ErrorMessage = "字段名称不能为空")]
    public string FieldName { get; set; }
}

#endregion

#region 输出参数

/// <summary>
/// 枚举输出参数
/// </summary>
public class EnumDataOutput
{
    /// <summary>
    /// 字典Id
    /// </summary>
    public int Code { get; set; }

    /// <summary>
    /// 字典值
    /// </summary>
    public string Value { get; set; }
}
#endregion
