﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysAppService : ITransient
{
    /// <summary>
    /// 分页查询系统应用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageList<SysApp>> PageList(QuerySysAppPageInput input);
    /// <summary>
    /// 获取应用详情
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<SysApp> Get(PrimaryKeyParam input);
    /// <summary>
    /// 获取应用列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<List<SysApp>> List();
    /// <summary>
    /// 获取用户应用相关信息
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    Task<List<SysApp>> GetLoginApps(long userId);
    /// <summary>
    /// 设为默认应用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task SetAsDefault(PrimaryKeyParam input);
    /// <summary>
    /// 新增应用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task Add(AddSysAppParam input);
    /// <summary>
    /// 删除应用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task Delete(Core.PrimaryKeyParam input);
    /// <summary>
    /// 编辑应用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task Update(EditSysAppParam input);
    /// <summary>
    /// 修改应用状态
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task ChangeStatus(ChangeSysAppStatusParam input);
}
