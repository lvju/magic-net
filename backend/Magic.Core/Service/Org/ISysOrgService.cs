﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysOrgService : ITransient
{
    Task Add(AddOrgInput input);
    Task Delete(Core.PrimaryKeyParam input);
    Task<List<long>> GetDataScopeListByDataScopeType(int dataScopeType, long orgId);
    Task<SysOrg> Get(Core.PrimaryKeyParam input);
    Task<List<OrgOutput>> List(QueryOrgPageInput input);
    Task<List<OrgTreeNode>> GetOrgTree();
    Task<PageList<OrgOutput>> PageList(QueryOrgPageInput input);
    Task Update(EditOrgInput input);
    Task<List<long>> GetAllDataScopeIdList();
}
