﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysFileService : ITransient
{
    Task Delete(PrimaryKeyParam input);
    Task<IActionResult> DownloadFileInfo(PrimaryKeyParam input);
    Task<SysFile> Get(PrimaryKeyParam input);
    Task<List<SysFile>> List(QueryFilePageInput input);
    Task<IActionResult> Preview(PrimaryKeyParam input);
    Task<PageList<QueryFilePageOutput>> PageList(QueryFilePageInput input);
    Task<long> UploadFileAvatar(IFormFile file);
    Task<string> UploadFileEditor(IFormFile file);
    Task UploadFileDefault(IFormFile file);
    Task UploadFileDocument(IFormFile file);
    Task UploadFileShop(IFormFile file);
}
