﻿using Aliyun.OSS;
using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Magic.Web.Core;

/// <summary>
/// 代码生成器服务
/// </summary>
[ApiDescriptionSettings(Name = "CodeGen", Order = 100, Tag = "代码生成器服务")]
public class CodeGenerateController : IDynamicApiController
{
    private readonly ICodeGenService _service;
    public CodeGenerateController(ICodeGenService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/codeGenerate/page")]
    public async Task<dynamic> QueryCodeGenPageList([FromQuery] QueryCodeGenPageInput input) { 
        return await _service.PageList(input);
    }

    /// <summary>
    /// 增加
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/codeGenerate/add")]
    public async Task AddCodeGen(AddCodeGenInput input) { 
        await _service.Add(input);
    }

    /// <summary>
    /// 删除
    /// </summary>
    /// <param name="inputs"></param>
    /// <returns></returns>
    [HttpPost("/codeGenerate/delete")]
    public async Task DeleteCodeGen(List<Magic.Core.PrimaryKeyParam> inputs) { 
        await _service.Delete(inputs);
    }

    /// <summary>
    /// 更新
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/codeGenerate/edit")]
    public async Task UpdateCodeGen(EditCodeGenInput input) { 
        await _service.UpdateCodeGen(input);
    }

    /// <summary>
    /// 详情
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/codeGenerate/detail")]
    public async Task<SysCodeGen> GetCodeGen([FromQuery] Magic.Core.PrimaryKeyParam input) { 
        return await _service.Get(input);
    }

    /// <summary>
    /// 获取数据库表(实体)集合
    /// </summary>
    /// <returns></returns>
    [HttpGet("/codeGenerate/InformationList")]
    public async Task<List<TableOutput>> GetTableList() { 
        return await _service.GetTableList();
    }

    /// <summary>
    /// 根据表名获取列
    /// </summary>
    /// <returns></returns>
    [HttpGet("/codeGenerate/ColumnList/{tableName}")]
    public List<TableColumnOutput> GetColumnListByTableName(string tableName) { 
        return _service.GetColumnListByTableName(tableName);
    }

    /// <summary>
    /// 代码生成_本地项目
    /// </summary>
    /// <returns></returns>
    [HttpPost("/codeGenerate/runLocal")]
    public async Task RunLocal(SysCodeGen input) { 
        await _service.RunLocal(input);
    }
}
