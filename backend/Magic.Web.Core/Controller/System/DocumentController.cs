﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 文档服务
/// </summary>
[ApiDescriptionSettings(Name = "Document", Order = 150, Tag = "文档服务")]
public class DocumentController : IDynamicApiController
{
    private readonly IDocumentService _service;
    public DocumentController(IDocumentService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询文档
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/Document/page")]
    public async Task<PageList<QueryDocumentPageOutput>> Page([FromQuery] QueryDocumentPageInput input) {
        return await _service.Page(input);
    }

    /// <summary>
    /// 文件夹树
    /// </summary>
    /// <returns></returns>
    [HttpGet("/Document/tree")]
    public async Task<List<DocumentTreeOutPut>> Tree() {
        return await _service.Tree();
    }

    /// <summary>
    /// 上传文件
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/Document/upload")]
    public async Task Upload([FromForm] DocumentUploadInput input) {
        await _service.Upload(input);
    }

    /// <summary>
    /// 上传文件夹
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/Document/uploadfolder")]
    public async Task UploadFolder([FromForm] DocumentUploadInput input) { 
        await _service.UploadFolder(input);
    }

    /// <summary>
    /// 新建文件夹
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/Document/add")]
    public async Task<long> Add(AddDocumentInput input)
    {
        return await _service.Add(input);
    }

    /// <summary>
    /// 删除文档
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/Document/delete")]
    public async Task Delete(Magic.Core.PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 批量删除
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/Document/deletes")]
    public async Task Deletes(BatchDeleteDocumentInput input)
    {
        await _service.Deletes(input);
    }

    /// <summary>
    /// 移动文档
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/Document/move")]
    public async Task Move(MoveDocumentInput input)
    {
        await _service.Move(input);
    }
    /// <summary>
    /// 更新文件夹
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/Document/edit")]
    public async Task Update(EditDocumentInput input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 获取文档
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/Document/detail")]
    public async Task<Documentation> Get([FromQuery] Magic.Core.PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 下载文件
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>

    [HttpGet("/Document/download")]
    public async Task<IActionResult> Download([FromQuery] Magic.Core.PrimaryKeyParam input)
    {
        return await _service.Download(input);
    }

    /// <summary>
    /// 预览文件
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/Document/preview")]
    public async Task<string> Preview([FromQuery] Magic.Core.PrimaryKeyParam input)
    {
        return await _service.Preview(input);
    }
}
