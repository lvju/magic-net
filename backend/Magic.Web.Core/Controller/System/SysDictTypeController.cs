﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Web.Core;


/// <summary>
/// 字典类型服务
/// </summary>
[ApiDescriptionSettings(Name = "DictType", Order = 100, Tag = "字典类型服务")]
public class SysDictTypeController : IDynamicApiController
{
    private readonly ISysDictTypeService _service;
    public SysDictTypeController(ISysDictTypeService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询字典类型
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysDictType/page")]
    public async Task<PageList<SysDictType>> PageList([FromQuery] QueryDictPageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 获取字典类型列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysDictType/list")]
    public async Task<List<SysDictType>> List()
    {
        return await _service.List();
    }

    /// <summary>
    /// 获取字典类型下所有字典值
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("/sysDictType/dropDown")]
    public async Task<List<SysDictData>> GetDictTypeDropDown([FromQuery] QueryDropDownDictTypeInput input)
    {
        return await _service.GetDictTypeDropDown(input);
    }

    /// <summary>
    /// 添加字典类型
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysDictType/add")]
    public async Task Add(AddDictTypeInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除字典类型
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysDictType/delete")]
    public async Task Delete(Magic.Core.PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新字典类型
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysDictType/edit")]
    public async Task Update(EditDictTypeInput input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 字典类型详情
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysDictType/detail")]
    public async Task<SysDictType> Get([FromQuery] Magic.Core.PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 更新字典类型状态
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysDictType/changeStatus")]
    public async Task ChangeDictTypeStatus(UpdateDictTypeStatusInput input)
    {
        await _service.ChangeDictTypeStatus(input);
    }

    /// <summary>
    /// 字典类型与字典值构造的字典树
    /// </summary>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("/sysDictType/tree")]
    public async Task<List<DictTreeOutput>> GetDictTree()
    {
        return await _service.GetDictTree();
    }
}
