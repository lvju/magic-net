﻿using Aliyun.OSS;
using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Magic.Web.Core;

/// <summary>
/// 文章服务
/// </summary>
[ApiDescriptionSettings(Name = "Article", Order = 100, Tag = "文章服务")]
public class SysArticleController : IDynamicApiController
{
    private readonly ISysArticleService _service;
    public SysArticleController(ISysArticleService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询文章
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysArticle/page")]
    public async Task<PageList<SysArticlePageOutput>> PageList([FromQuery] QuerySysArticleInput input)
    {
        return await _service.PageList(input);
    }


    /// <summary>
    /// 增加文章
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysArticle/add")]
    public async Task Add(AddSysArticleInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除文章
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysArticle/delete")]
    public async Task Delete(Magic.Core.PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新文章
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysArticle/edit")]
    public async Task Update(EditSysArticleInput input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 获取文章详情
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysArticle/detail")]
    [AllowAnonymous]
    public async Task<SysArticleDetailOutput> Get([FromQuery] Magic.Core.PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }
}
