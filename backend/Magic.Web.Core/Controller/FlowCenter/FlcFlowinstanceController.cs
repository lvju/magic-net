﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.FlowCenter.Entity;
using Magic.FlowCenter.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 工作流服务
/// </summary>
[ApiDescriptionSettings("FlowCenter", Name = "FlcFlowinstance", Order = 100, Tag = "工作流服务")]
public class FlcFlowinstanceController : IDynamicApiController
{
    private readonly IFlcFlowinstanceService _service;
    public FlcFlowinstanceController(IFlcFlowinstanceService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询工作流
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/flcFlowinstance/page")]
    public async Task<PageList<FlcFlowinstance>> Page([FromQuery] QueryFlcFlowinstancePageInput input)
    {
        return await _service.Page(input);
    }

    /// <summary>
    /// 获取工作流
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("/flcFlowinstance/detail")]
    public async Task<FlcFlowinstanceOutput> Get(long id)
    {
        return await _service.GetForVerification(new PrimaryKeyParam { Id = id });
    }

    /// <summary>
    /// 获取工作流列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>Verification
    [HttpGet("/flcFlowinstance/list")]
    public async Task<List<FlcFlowinstance>> List([FromQuery] QueryFlcFlowinstancePageInput input)
    {
        return await _service.List(input);
    }

    /// <summary>
    /// 获取历史
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/flcFlowinstance/histories")]
    public async Task<List<FlcFlowInstanceOperationHistory>> QueryHistories([FromQuery] PrimaryKeyParam input)
    {
        return await _service.QueryHistories(input);
    }

    /// <summary>
    /// 节点处理
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/flcFlowinstance/verification")]
    public async Task Verification(Verification input)
    {
        await _service.Verification(input);
    }

    /// <summary>
    /// 新增流程实例
    /// </summary>
    /// <param name="entity"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    [HttpPost("/flcFlowinstance/add")]
    public async Task Add(FlcFlowinstance entity)
    {
        await _service.Add(entity);
    }

    /// <summary>
    /// 编辑流程实例
    /// </summary>
    /// <param name="entity"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    [HttpPost("/flcFlowinstance/edit")]
    public async Task Update(FlcFlowinstance entity)
    {
        await _service.Update(entity);
    }

    /// <summary>
    /// 删除流程实例
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/flcFlowinstance/delete")]
    public async Task Delete(PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 撤销流程实例
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/flcFlowinstance/cancel")]
    public async Task Cancel(PrimaryKeyParam input)
    {
        await _service.Cancel(input);
    }
}
